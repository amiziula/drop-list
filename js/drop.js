/**
 * Dropdown jQuery plugin
 * @author Adrian
 */
;(function($){
	$.fn.drop = function(options){
		options = $.extend({
			duration: 200
		}, options);
		var selector = this.selector;
		return $(this).each(function(){
			var self = $(this);
			var label = self.find('.drop-label');
			var list = self.find('.drop-list');
			
			// add to list empty value
			list.prepend('<li/>');
			
			// hide drop list if clicked outside element
			$(document).click(function(){
				if (list.is(':visible')){
					list.slideUp(options.duration);
				}
			});
			
			// show drop list
			label.click(function(e){
				if (list.is(':hidden')){
					$(selector + ' .drop-list').hide();
					list.slideDown(options.duration);
				} else {
					list.slideUp(options.duration);
				}
				e.stopPropagation();
			});
			
			var listItem = list.find('li');
			
			// chosen value from drop list
			listItem.click(function(){
				var active = $(this);
				active.siblings('li').removeClass('selected');
				active.addClass('selected');
				var selectedText = active.text();
				label.text(selectedText);
				list.hide();
			});
			
			// if element is selected by default
			if (listItem.is('.selected')){
				list.find('li.selected').trigger('click');
			}
			
		});
	};
})(jQuery);
